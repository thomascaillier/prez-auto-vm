---
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
---

# Déployer un environnement de test en 15 minutes

Pour gagner en efficacité, en qualité et en adaptabilité

---

# Dis, grand-père, c'était comment avant ?

Pour le test et la qualification, on utilisait des environnements globaux à l'iMSA. Dans notre cas, Essai et SLE.
Les inconvénients ?

### Ressources
10 000 DSN à intégrer... hum
### Disponibilité
pannes fréquentes, tickets ECO/SOLIQ
### Intégrité
comment ont été alimentés les référentiels ? par qui ? 
### Un pour tous, tous pour un
ah, tu viens de purger toutes les tables ?

---
layout: image-right
image: https://source.unsplash.com/collection/94734566/1920x1080
---

# Ok, on change. Mais où ?

## e-sandbox
- https://e-sandbox.agora.msanet/vcac/org/e-sandbox/
- Service de cloud privé de l'iMSA
- Permet de créer et mettre à disposition une VM en 15 minutes.
- à vocation de POOC

<!-- Démo d'une demande de CentOS Docker sur le portail e-sandbox -->

## en cible : les EEE
Environnements d'Execution Ephémères

---

# On fait quoi ?
En un clic *(à peu près...)* depuis un poste Windows :
- Une VM configurée
- Un ensemble applicatif déployé
- Des référentiels chargés

Autrement dit, un environnement de test prêt à chauffer.

<!-- Lancement du script -->

---

# On fait comment ?
- Docker & docker-compose
  - https://git.agora.msanet/sidl/kdsenv-config
  - Un .env pour les versions applicatives, déclarations de services et configurations
  - Des services git & svn pour récupérer les sources
  - Des services maven pour les compiler
  - Des services postgres pour les BDD
  - Un service JBoss par silo pour être iso prod

---

# On fait comment ? 
- Un launcher Windows qui envoie des fichiers et lance des commandes sur la VM :
  - https://git.agora.msanet/sidl/outils/esandbox-sidl-toolkit
  - configuration du proxy
  - configuration de Docker
  - paramétrage du swap
  - clone de kdsenv-config
  - déployer des RuleApp ODM (moteur de règles)
  - (optionnel) lancement des référentiels
  - lancement des docker-compose
  - rapport de déploiement

<!-- On montre le résultat -->

---

# Et en bonus
- Nouvelles opportunités :
  - outils dédiés à l'automatisation de test
  - stack kibana/elasticsearch
  - gestionnaire de versions


